export interface DialogflowResponseBody {
    fulfillmentText?: string
    basicCard?: {
        image?: {
            url: string
        }
    }
    
}
