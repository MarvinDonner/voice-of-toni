/**
 * Mappt Strings auf T
 */
export type Map<T> = { [key: string]: T };
