export interface  DialogflowRequestBody {
    responseId: string
    session: string
    queryResult: {
        queryText: string
        action: string

        parameters: Parameters
        outputContexts?: Context[]
    }
}

export interface Parameters {
    date?: string
}

export type Context = any
