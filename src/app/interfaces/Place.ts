export interface Place {
    /** WO-interne Geo ID */
    geoID: string
    /** Name des Ortes, der zur Geo ID gehört */
    geoName: string
    /**  Breitengrad (Dezimalgrad) */
    latitude: number
    /** Längengrad (Dezimalgrad) */
    longitude: number
    /** Höhe über NN */
    altitude?: number

    /** Name des Ortes */
    locationName: string
    /** Name des Unterortes */
    subLocationName: string

    /** ID des Bundeslandes */
    subStateID: string
    /** Name des Bundeslandes */
    subStateName: string

    /** ID des Staates */
    stateID: string
    /** Name des Staates */
    stateName: string

    /** Offset zu UTC */
    utcOffset?: number
    /** Name der Zeitzone */
    timeZone: string

    /** URL zur www Landingpage */
    url?: string

    /** Postleitzahl */
    zipCode?: string

}

/**
 * Ein einzelnes Suchergebnis
 */
export interface SearchResult extends Place {
    prio: number
    server: string
    serverKey: string
}

/**
 * Array von {@link SearchResult}
 */
export type SearchResults = SearchResult[];
