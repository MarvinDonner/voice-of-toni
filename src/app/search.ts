import fetch, {Response} from "node-fetch";
import {StringDecoder} from "string_decoder";
import moment = require("moment");
import {userInfo} from "os";
import {Map} from "./interfaces/Map";
import {Place, SearchResults} from "./interfaces/Place";
import {fetchWithError} from "./fetch";
// import {BACKEND_URL} from "../constants";

export let api_search_call_LoggingObject: Api_Search_Call_LoggingObject;

export interface Api_Search_Call_LoggingObject {
    requestTime: number
    url: string
}

const BACKEND_URL = `https://api.wetteronline.de`;
//const BACKEND_URL = "https://api" + IS_DEVELOPMENT_SUFFIX + ".wetteronline.de";
/**
 * Gibt an wie das Ergebnis einer Suche interpretiert werden sollte
 */
export enum SearchResultType {
    /** eine eindeutige Stadt wurde gefunden */
    DistinctCity,

    /** eine eindeutige Fläche wurde gefunden */
    DistinctArea,

    /** Ergebnis ist Kontinent */
    Continent,

    /** Nicht eindeutige Suche, mehrere Städte gefunden */
    NotDistinct,

    /** Nicht eindeutige Suche, mehrere Städte innerhalb Deutschlands gefunden */
    NotDistinctGermany,

    /** Der Ort mag existieren, jedoch nicht in dem angegebenen Land */
    LocationNotInCountry,

    /** Nichts gefunden */
    NotFound
}

/** Enthaltene Wörter geben automatisch {@link SearchResultType.NotFound} bei einer Suche zurück */
const searchBlacklist                   = ["morgen"];
/** Ersetzt gefundene Suchnamen durch einen korrigierten Namen */
const searchReplacementMap: Map<string> = {
    "majorca":  "mallorca",
    " majorca": "mallorca"
};

/**
 * Ergebnis einer Suche.
 * Enthält den Typ des Ergebnisses und 0..n Ergebnisse
 */
export type SearchResult = {
    type: SearchResultType,
    query: string,
    results: Place[]
}

/**
 * Ruft a_search auf api[-dev].wetteronline.de auf. Gesucht wird nach einem Namen
 * @param {string} name Suchname
 * @param {string} language Sprache, in der gesucht werden soll
 * @param {string} country Falls übergeben, soll nur in diesem Land gesucht werden
 * @returns {Promise<SearchResults>}
 * @throws Fehler bei Verbindungsproblemen oder einer fehlerhaften Antwort
 */
async function apiNameSearch(name: string, country?: string, areaCities = true): Promise<SearchResults> {
    const countryQuery = country ? `&country=${encodeURIComponent(country)}` : "";
    const areaCitesQuery = areaCities ? "&area=cities" : "";
    const url = `${BACKEND_URL}/search?name=${encodeURIComponent(name)}&application=voiceassistant&mv=1${countryQuery}&function=url${areaCitesQuery}`;

    api_search_call_LoggingObject = {
        requestTime: 0,
        url: url
    };

    return fetchWithError(url, {
        method: "GET",
    })
      .then(res => res.json());
}

/**
 * Ruft a_search auf api[-dev].wetteronline.de auf. Gesucht wird nach Koordinaten
 * @param {number} lat Latitude / Breitengrad
 * @param {number} lon Longitude / Längengrad
 * @param {string} language Sprache, in der gesucht werden soll
 * @returns {Promise<SearchResults>}
 */
async function apiCoordinateSearch(lat: number, lon: number): Promise<SearchResults> {
    const latStr = "" + Math.round(lat * 100) / 100;
    const lonStr = "" + Math.round(lon * 100) / 100;
    const url = `${BACKEND_URL}/search?lat=${latStr}&lon=${lonStr}&application=voiceassistant&mv=1&function=url`;

    api_search_call_LoggingObject = {
        requestTime: 0,
        url: url
    };

    return fetchWithError(url, {
        method: "GET",
    })
      .then(res => res.json());
}

/**
 * Ruft a_seach auf api[-dev].wetteronline.de auf. Gesucht wird nach einem Namne
 * @param {RuntimeContext} ctx Kontext
 * @param {string} name Query
 * @param {string} stateNameFilter [optional] Region, nach der gefiltert werden soll
 * @param {boolean} alsoSearchForState [Default: false] gibt an, ob zuerst nach dem stateNameFilter gesucht werden soll
 * @param {boolean} areaCities Suchfilter, dass nur nach Städten gesucht werden soll
 * @param {boolean} retry bearbeitet die Query bei Nichtfinden und versucht das ganze erneut
 * @returns {Promise<SearchResult>}
 */
export async function searchByName(name: string, stateNameFilter?: string, alsoSearchForState = false, areaCities = true, retry = false): Promise<SearchResult> {
    const  now        = +new Date();

    if (name in searchReplacementMap) {
        name = searchReplacementMap[name];
    }

    const out: SearchResult = {
        type:    SearchResultType.NotFound,
        results: [],
        query: name
    };

    if (searchBlacklist.indexOf(name) > -1) {
        return out;
    }

    try {
        let useStateFilter = !!stateNameFilter;
        let stateName      = "";

        if (useStateFilter && alsoSearchForState) {
            const stateRes = await searchByName(stateNameFilter as string, undefined, alsoSearchForState, areaCities);
            switch (stateRes.type) {
                case SearchResultType.NotFound:
                    out.type = SearchResultType.NotFound;
                    return out;
                case SearchResultType.DistinctArea:
                    stateName = stateRes.results[0].locationName;
                    break;
                default:
                    useStateFilter = false;
                    break;
            }
        }

        //console.time("Api-Search-Call - searchByName");
        const searchResults = out.results = await apiNameSearch(name, stateName, areaCities);
        //console.timeEnd("Api-Search-Call - searchByName");
        out.type = resultsToType(searchResults);

        if(out.type === SearchResultType.NotFound && retry) {
            if(name.includes(" in ")) {
                return await searchByName(name.replace(" in ", ", "), stateNameFilter, alsoSearchForState, areaCities, false);
            } else if(name.includes(" ")) {
                return await searchByName(name.substr(0, name.indexOf(" ")), stateNameFilter, alsoSearchForState, areaCities, false);
            }
        }

        if (out.type === SearchResultType.NotDistinct) {
            out.results = cleanResultList(searchResults);
            out.type    = resultsToType(<SearchResults> out.results);
        }

        if(out.type === SearchResultType.NotFound && useStateFilter) {
            out.type = SearchResultType.LocationNotInCountry;
        }
    }
    catch (e) {
        out.type = SearchResultType.NotFound;
    }

    return out;
}

/**
 * Ruft a_search auf api[-dev].wetteronline.de auf und sucht nach Koordinaten
 * @param {RuntimeContext} ctx der Kontext
 * @param {number} lat Breitengrad
 * @param {number} lon Längengrad
 * @returns {Promise<SearchResult>}
 */
export async function searchByCoordinate(lat: number, lon: number): Promise<SearchResult> {
    const now = +new Date();

    let type                   = SearchResultType.NotFound;
    let results: SearchResults = [];
    try {
        results = filterResults(await apiCoordinateSearch(lat, lon));
        console.log(results);
        type    = resultsToType(results);
    }
    catch (e) {
        type = SearchResultType.NotFound;
    }
    return {
        type,
        results,
        query: `${lat},${lon}`
    };

    function filterResults(locations: SearchResults): SearchResults {
        if(locations.length === 1) return locations;
        let index = 0;
        let diff: number | null = null;
        let resultIndex = 0;

        for(const location of locations) {
            const currentDiff = getCoordinateDistance(lat, lon, location.latitude, location.longitude);

            if(diff === null || currentDiff < diff) {
                resultIndex = index;
                diff = currentDiff;
            }

            index++;
        }

        return [locations[resultIndex]];
    }

    function getCoordinateDistance(lat1: number, lon1: number, lat2: number, lon2: number) {
        const R = 6371; // Radius of the earth in km
        const dLat = deg2rad(lat2-lat1);  // deg2rad below
        const dLon = deg2rad(lon2-lon1);
        const a =
          Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
          Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R * c * 1000;
    }

    function deg2rad(deg: number) {
        return deg * (Math.PI/180);
    }
}

/**
 * Wandelt einen Ort in einen Base64-String um
 * @param {Place} location der Ort
 * @returns {string}
 */
export function locationToBase64(location: Place) : string {
    return new Buffer(JSON.stringify(location as any)).toString("base64");
}

/**
 * Wandelt einen Base64 in einen String um
 * @param {string} input der Base64-String
 * @returns {Place}
 */
export function base64ToLocation(input: string) : Place {
    return JSON.parse(new Buffer(input, "base64").toString("utf-8")) as Place;
}

/**
 * Gibt den Ortsnamen eines Place-Objektes aus (z.B. "Graurheindorf (Bonn)")
 * @param {Place} location
 * @returns {string}
 */
export function getLocationName(location: Place) : string {
    return !!location.subLocationName ? `${location.subLocationName} (${location.locationName})` : location.locationName;
}

/**
 * Gibt an, ob die Orte gleich sind
 * @param {Place} locations
 * @returns {boolean}
 */
export function locationsEqual(...locations: Array<Place>): boolean {
    let ret = true;
    let first = true;

    if(locations.length <= 1) return true;
    for(const location of locations) {
        if(first) {
            first = false;
        } else {
            ret = ret && (
              location.locationName === locations[0].locationName &&
              location.geoID === locations[0].geoID
            );
            if(!!location.subLocationName && !!locations[0].subLocationName) {
                ret = ret && location.subLocationName === locations[0].subLocationName;
            } else {
                ret = ret && !!location.subLocationName === !!locations[0].subLocationName;
            }
            if(!ret) return ret;
        }
    }

    return ret;
}

export interface FurtherLocationInformation {
    isInGermany: boolean
    isState: boolean
    isSubState: boolean
    useSubState: boolean
    isRegion: boolean
    isFederal: boolean
    isCity: boolean
}

/**
 * Ermittelt aus dem Place-Objekt weitere Informationen:
 * Ist der Ort Deutsch, ist er ein (Bundes-)-Land und kann der SubState genutzt werden
 * @param {Place} location
 * @returns {FurtherLocationInformation}
 */
export function getFurtherLocationInformation(location: Place) : FurtherLocationInformation {
    const isInGermany = location.stateID === "DL";
    const useSubState = isInGermany && !!location.subStateName;
    const isState = location.geoID.length === 2 && location.locationName === location.stateName;
    const isSubState = location.geoID.length === 3 && location.locationName === location.subStateName;
    const isRegion = location.geoID.length < 5;
    const isFederal = location.geoID.length === 5 && (location.locationName === location.stateName || location.locationName === location.subStateName);
    const isCity = location.geoID.length === 5;

    return {
        isInGermany,
        useSubState,
        isSubState,
        isState,
        isRegion,
        isFederal,
        isCity
    };
}

function resultsToType(results: SearchResults): SearchResultType {
    if (!results.length) {
        return SearchResultType.NotFound;
    }
    if (results.length === 1) {
        if (geoIDIsCity(results[0].geoID)) {
            return SearchResultType.DistinctCity;
        } else if (results[0].geoID.length === 4) {
            return SearchResultType.Continent;
        } else {
            return SearchResultType.DistinctArea;
        }
    }

    for (const res of results) {
        if (res.stateID !== "DL") {
            return SearchResultType.NotDistinct;
        }
    }

    return SearchResultType.NotDistinctGermany;
}

function cleanResultList(results: SearchResults) {
    const out: SearchResults  = [];
    const countries: string[] = [];
    let containsCity          = searchResultsContainCity(results);

    for (const res of results) {
        if (containsCity && !geoIDIsCity(res.geoID)) {
            continue;
        }
        if (countries.includes(res.stateName + res.locationName) && res.stateID !== "DL") {
            continue;
        }

        out.push(res);
        countries.push(res.stateName + res.locationName);
    }

    return out;
}

/**
 * Gibt <b>true</b> zurück, wenn in den übergebenen Ergebnissen wenigstens eine Stadt enthalten ist
 * @param {SearchResults} results
 * @returns {boolean}
 */
function searchResultsContainCity(results: SearchResults) {
    for (let res of results) {
        if (geoIDIsCity(res.geoID)) {
            return true;
        }
    }
    return false;
}

/**
 * Gibt <b>true</b> zurück, wenn die übergebene GeoID die einer Stadt ist
 * @param {string} geoID
 * @returns {boolean}
 */
function geoIDIsCity(geoID: string) {
    return !!(geoID && geoID.length === 5);
}


export function isValidLocationObj(obj: Place): boolean {
    const tmp = <any> obj;
    return !!tmp.geoID && !!tmp.locationName && !!tmp.latitude && !!tmp.longitude;
}
