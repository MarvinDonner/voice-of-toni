import { DialogflowRequestBody } from "./interfaces/RequestBody";
import { DialogflowResponseBody } from "./interfaces/ResponseBody";


export class VoiceHandler {

    /** Hier stehen die Request Daten drin */
    private dialogflowRequest: DialogflowRequestBody;

    constructor(private data: any) {
        this.dialogflowRequest = data;
    }

    //TODO: any sagt dem Compiler, dass ALLES in der Response erlaubt ist.
    //TODO: Besser: Ihr definiert in DialogflowResponseBody was in eurer Response erlaubt ist. Sozusagen das payload --> google --> richResponse usw. Schema aus getLogo
    public async handle(): Promise<any> {//Promise<DialogflowResponseBody> {

        switch (this.dialogflowRequest.queryResult.action) {
            case "input.welcome":
                return this.getWelcome();
            case "action.logo":
                return this.getLogo();
                //break;
        }

        return {
            fulfillmentText: "Ich verstehe dich leider nicht. "
        };

    }

    public getWelcome() {
        try {

            return {
                fulfillmentText: "<speak>Ich heiße Toni!</speak>"
            };

        } catch (e) {

            console.error("e: " + e);
            return {};
        }
    }

    public getLogo() {

        return {
            "payload": {
                "google": {
                    "richResponse": {
                        "items": [
                            {
                                "simpleResponse": {
                                    "textToSpeech": "Logo" //TODO: Hier hat nur der Text gefehlt. Sonst hätte es beim letzten Termin gefehlt
                                },
                            },
                            {
                                "htmlResponse": {
                                    "url": "https://canvas-firebase-43b0c.firebaseapp.com" //TODO: Hier dreht sich ein Dreieck. Siehe auch Browser. Hier kann dann eine beliebige URL oder auch ein localhost über ngrok übergeben werden.
                                }
                            },
                        ]
                    }
                }
            }
        }
    }
}