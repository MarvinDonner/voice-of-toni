import {RequestInit, Response} from "node-fetch";
import fetch from "node-fetch";

/**
 * Fehler bei Aufruf eines Fetch Requests
 */
export interface FetchError {
    status: number
    statusText: string
    url: string
    content?: string
}

async function catchError(r: Response) {
    if (r.status >= 200 && r.status < 300) {
        return r;
    }
    const e: FetchError = {
        status:     r.status,
        statusText: r.statusText,
        url:        r.url,
    };
    try {
        e.content = await r.text();
    }
    catch (err) {
    }
    throw e;
}

/**
 * Startet einen fetch Request, der einen Fehler wirft, wenn der Statuscode nicht 2xx ist
 * @param {string} url Request URL
 * @param {RequestInit} init
 * @returns {Promise<Response>}
 * @throws FetchError
 */
export function fetchWithError(url: string, init?: RequestInit) {
    return fetch(url, init)
      .then(catchError);
}

/**
 * Startet einen fetch Request mit der Methode POST
 * @param {string} url Request URL
 * @param body Body für den Request
 * @param {RequestInit} init
 * @returns {Promise<Response>}
 * @throws FetchError
 */
export function postFetch(url: string, body: any, init: RequestInit = {}) {
    init.method = "POST";
    init.body   = typeof body === "string" ? body : JSON.stringify(body);
    if (!init.headers) {
        init.headers = {};
    }

    (init.headers as { [index: string]: string })['Content-Type'] = (init.headers as { [index: string]: string })['Content-Type'] || 'application/json';

    return fetchWithError(url, init);
}
