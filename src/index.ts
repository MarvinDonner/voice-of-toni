import express = require("express");
import {Request, Response} from "express";
import * as bodyParser from "body-parser";
import {VoiceHandler} from "./app/VoiceHandler";
import {Map} from "./app/interfaces/Map";

const port = 3000;
const app = express();

app.use(bodyParser.json());
app.post("/dialogflow", async(req: Request, res: Response) => {

    //Test
    try {
        const data = req.method === "GET" ? req.query : req.body;
        console.log("\n\n Request: " + JSON.stringify(data) + "\n\n");

        const handler = new VoiceHandler(data);
        const responseBody = await handler.handle();

        console.log("\n\n Response: " + JSON.stringify(responseBody) + "\n\n");
        res
            .status(200)
            .send(JSON.stringify(responseBody));

    } catch (e) {
        console.error("e: " + JSON.stringify(e));
        res.sendStatus(500);
    }

});

app.get("/test", (req, res) => res.send("OK"));


app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});


function errorToString(err: any, escapeStack = false) {
    if(typeof err === "string") return onlyOneBackslash(err);

    const tmp: Map<any> = {};
    for(const key of Object.keys(err)) {
        tmp[key] = err[key];
    }

    if(Object.keys(tmp).length === 0) {
        for(const attribute of ["message", "callstack", "status", "statusText", "url", "content", "msg", "stack"]) {
            if(typeof err[attribute] !== "undefined") {
                tmp[attribute] = err[attribute]
            }
        }
    }

    if(escapeStack && tmp.stack) tmp.stack = escapeCallStack(tmp.stack);
    if(escapeStack && tmp.callstack) tmp.callstack = escapeCallStack(tmp.callstack);

    return onlyOneBackslash(JSON.stringify(tmp));
}

function onlyOneBackslash(str: string): string {
    if(str.length === 0) return str;
    let ret = str.charAt(0);

    for(let i = 1; i < str.length; i++) {
        const chr = str.charAt(i);
        if(chr === '\\' && chr === ret.charAt(ret.length-1)) continue;

        ret += chr;
    }

    return ret;
}

function escapeCallStack(callStack: string) {
    return callStack.replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .split("\n")
      .join("<br>")
      .replace(" ", "&nbsp;");
}
